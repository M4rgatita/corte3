
  // Import the functions you need from the SDKs you need
  import { initializeApp } from "https://www.gstatic.com/firebasejs/9.13.0/firebase-app.js";
  import{getDatabase,onValue,ref,set,child,get,update,remove}  
  from "https://www.gstatic.com/firebasejs/9.13.0/firebase-database.js";
  // TODO: Add SDKs for Firebase products that you want to use
  // https://firebase.google.com/docs/web/setup#available-libraries

  // Your web app's Firebase configuration
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  const firebaseConfig = {
    apiKey: "AIzaSyAtyVaImQfS36IhQxa_1eNWm6_BYTJUM6k",
    authDomain: "uvuweb-ffecc.firebaseapp.com",
    databaseURL: "https://uvuweb-ffecc-default-rtdb.firebaseio.com",
    projectId: "uvuweb-ffecc",
    storageBucket: "uvuweb-ffecc.appspot.com",
    messagingSenderId: "811905934331",
    appId: "1:811905934331:web:ba075a200b737c21e9a15e",
    measurementId: "G-V7VVKC2RDF"
  };

  // Initialize Firebase
  const app = initializeApp(firebaseConfig);
  const db = getDatabase();


  var btnInsertar = document.getElementById("btnInsertar");
  var btnBuscar = document.getElementById("btnBuscar");
  var btnActualizar = document.getElementById("btnActualizar");
  var btnBorrar = document.getElementById("btnBorrar");
  var btnTodos = document.getElementById("btnTodos");
  var lista = document.getElementById("lista");
  var btnLimpiar = document.getElementById('btnLimpiar');
 // Insertar
 var matricula = "";
  var nombre = "";
  var carrera = "";
  var genero = "";
  function leerInputs(){
  matricula = document.getElementById("matricula").value;
  nombre = document.getElementById("nombre").value;
  carrera = document.getElementById("carrera").value;
  genero = document.getElementById('genero').value;
 
  }
 function insertDatos(){
 leerInputs();
  var genero= document.getElementById("genero").value;
  set(ref(db,'alumnos/' + matricula),{
  nombre: nombre,
  carrera:carrera,
  genero:genero})
  .then((docRef) => {
    alert("Se ha registrado exitosamente");
    mostrarAlumnos();
    console.log("datos" + matricula + nombre + carrera+ genero)
    })
    .catch((error) => {
    alert("Error en el registro")
    });
   
   
    alert (" Se ha agregado");
   
   
   };
   // mostrar datos
   function mostrarAlumnos(){
   const db = getDatabase();
   const dbRef = ref(db, 'alumnos');
   onValue(dbRef, (snapshot) => {
    lista.innerHTML=""
    snapshot.forEach((childSnapshot) => {
    const childKey = childSnapshot.key;
    const childData = childSnapshot.val();
   
    lista.innerHTML = "<div class='campo'> " + lista.innerHTML + " Matricula: " + childKey +
    " - Nombre: " + childData.nombre + " - Carrera: " + childData.carrera +" - Género: " + childData.genero +"<br> </div>";
    console.log(childKey + ":");
    console.log(childData.nombre)
    // ...
    });
   }, {
    onlyOnce: true
   });
   }
   function actualizar(){
    leerInputs();
   update(ref(db,'alumnos/'+ matricula),{
    nombre:nombre,
    carrera:carrera,
    genero :genero
}).then(()=>{
 alert("Se ha realizado la actualización del registro");
 mostrarAlumnos();
})
.catch(()=>{
 alert("causo Erro " + error );
});
}
function escribirInpust(){
 document.getElementById('matricula').value= matricula
 document.getElementById('nombre').value= nombre;
 document.getElementById('carrera').value= carrera;
 document.getElementById('genero').value= genero;
}
function borrar(){
 leerInputs();
 remove(ref(db,'alumnos/'+ matricula)).then(()=>{
 alert("Se ha borrado el registro");
 mostrarAlumnos();
 })
 .catch(()=>{
 alert("causo Erro " + error );
 });


}
function mostrarDatos(){
 leerInputs();
 console.log("mostrar datos ");
 const dbref = ref(db);
 get(child(dbref,'alumnos/'+ matricula)).then((snapshot)=>{
 if(snapshot.exists()){
 nombre = snapshot.val().nombre;
 carrera = snapshot.val().carrera;
 genero = snapshot.val().genero;
 console.log(genero);
 escribirInpust();
}
 else {
    alert("No existe");
 }
}).catch((error)=>{
 alert("error buscar" + error);
});
}
function limpiar(){
 lista.innerHTML="";
 matricula="";
 nombre="";
 carrera="";
 genero=1;
 escribirInpust();
}
btnInsertar.addEventListener('click',insertDatos);
btnBuscar.addEventListener('click',mostrarDatos);
btnActualizar.addEventListener('click',actualizar);
btnBorrar.addEventListener('click',borrar);
btnTodos.addEventListener('click', mostrarAlumnos);
btnLimpiar.addEventListener('click', limpiar);